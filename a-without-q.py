
import tkinter as tk
import tkinter.ttk as ttk
import csv


class Application(tk.Tk):
    def __init__(a, zero=None):
        super().__init__(zero)
        a.zero = zero
        a.frame = tk.Frame(a, height=1080, width=1920)
        a.frame.pack()
       
##        a.canvas1 = tk.Canvas(a, bg="#476581",
##                                            width = 210, height = 1920)
##        a.canvas1.pack()
##        a.canvas1.place(x=0, y=0)

        import csv

        a.food_database = []
        with open('good_food.csv') as file:
            file_csv = csv.reader(file)
        ##    something = next(file_csv)
            for row in file_csv:
                try:
##                    print(row[0])
                    a.food_database.append(row[0])
                except IndexError:
                    next

##        a.food_database = ['rice', 'steak', 'oranges' ]
##        print(type(a.food_database))

        a.create_widgets()
    
        entry = tk.Entry(a)
        entry.pack()
        entry.bind('<KeyRelease>', a.on_keyrelease)
        entry.place(x=25, y=35)

        a.listbox = tk.Listbox(a, height=800)
        a.listbox.pack()
        #listbox.bind('<Double-Button-1>', on_select)
        a.listbox.bind('<<ListboxSelect>>', a.on_select)
        a.listbox_update(a.food_database)
        a.listbox.place(x=25, y=60)

        btns_x = 300
        export_button = tk.Button(a, text = 'Export', command=a.export)
        export_button.configure(width = 10, activebackground = "#33B5E5",
                          bg='#476581', font=("Helvetica", 10))    
        export_button.place(x=btns_x, y=60)
        export_button.config(highlightthickness=0, fg='white')

        button2 = tk.Button(a, text = 'New Meal', command=a.new_meal_v2)
        button2.configure(width = 10, activebackground = "#33B5E5",
                              bg='#476581', font=("Helvetica", 10))
        
##            button1_window = a.canvas1.create_window(35, 10 + increment, anchor=NW, window=button1)
##        button1_window = a.canvas1.create_window(100, 25,  window=button1)
        button2.place(x=btns_x, y=110)
        button2.config(highlightthickness=0, fg='white')

        button1 = tk.Button(a, text = 'Add Item', command=a.add_item_v2)
        button1.configure(width = 10, activebackground = "#33B5E5",
                          bg='#476581', font=("Helvetica", 10))
    
##            button1_window = a.canvas1.create_window(35, 10 + increment, anchor=NW, window=button1)
##        button1_window = a.canvas1.create_window(100, 25,  window=button1)
        button1.place(x=btns_x, y=160)
        button1.config(highlightthickness=0, fg='white')

        button3 = tk.Button(a, text = 'Remove Item', command=a.delete)
        button3.configure(width = 10, activebackground = "#33B5E5",
                          bg='#476581', font=("Helvetica", 10))
    
##            button1_window = a.canvas1.create_window(35, 10 + increment, anchor=NW, window=button1)
##        button1_window = a.canvas1.create_window(100, 25,  window=button1)
        button3.place(x=btns_x, y=210)
        
        button3.config(highlightthickness=0, fg='white')

        
        button4 = tk.Button(a, text = 'Edit', command=a.edit)
        button4.configure(width = 10, activebackground = "#33B5E5",
                          bg='#476581', font=("Helvetica", 10))
    
##            button1_window = a.canvas1.create_window(35, 10 + increment, anchor=NW, window=button1)
##        button1_window = a.canvas1.create_window(100, 25,  window=button1)
        button4.place(x=btns_x, y=260)
        button4.config(highlightthickness=0, fg='white')

        button_back_meal = tk.Button(a, text = 'Back a Meal', command=a.back_a_meal)
        button_back_meal.configure(width = 10, activebackground = "#33B5E5",
                          bg='#476581', font=("Helvetica", 10))

        button_history = tk.Button(a, text = 'Log', command=a.history)
        button_history.configure(width = 10, activebackground = "#33B5E5",
                          bg='#476581', font=("Helvetica", 10))
        button_history.place(x=1000, y=5)
        button_history.config(highlightthickness=0, fg='white')

        
        button4.place(x=btns_x, y=360)
        button4.config(highlightthickness=0, fg='white')
    
##            button1_window = a.canvas1.create_window(35, 10 + increment, anchor=NW, window=button1)
##        button1_window = a.canvas1.create_window(100, 25,  window=button1)
        button_back_meal.place(x=btns_x, y=310)
        button_back_meal.config(highlightthickness=0, fg='white')


        button_clear_all = tk.Button(a, text = 'Clear All', command=a.clear_all)
        button_clear_all.configure(width = 10, activebackground = "#33B5E5",
                          bg='#476581', font=("Helvetica", 10))
        button_clear_all.place(x=btns_x, y=260)
        button_clear_all.config(highlightthickness=0, fg='white')

        button_add_item_to_spreadsheet = tk.Button(a, text = 'Add to Spreadsheet', command=a.spreadsheet)
        button_add_item_to_spreadsheet.configure(width = 15, activebackground = "#33B5E5",
                          bg='#476581', font=("Helvetica", 10))
    
##            button1_window = a.canvas1.create_window(35, 10 + increment, anchor=NW, window=button1)
##        button1_window = a.canvas1.create_window(100, 25,  window=button1)
        button_add_item_to_spreadsheet.place(x=280, y=415)
        button_add_item_to_spreadsheet.config(highlightthickness=0, fg='white')

        a.entry_spreadsheet = tk.Entry(a)
        a.entry_spreadsheet.pack()
        a.entry_spreadsheet.place(x=280, y=460)
       
        a.items = []
        a.meal1_items = []
        a.meal2_items = []
        a.meal3_items = []
        a.meal4_items = []
        a.meal5_items = []
        a.meal6_items = []
        
        a.meal8_items = []
        
        
        a.itemsd = {}
        
        a.incre_var = 0
        a.increment = 0

        from time import gmtime, strftime, localtime

##        time = strftime("%Y-%m-%d %H:%M:%S", localtime())
        a.time = strftime("%Y-%m-%d %H:%M:%S", localtime())

        a.label_time = tk.Label(a, text='Today is ' + a.time, fg="#476581")
        a.label_time.pack()
        a.label_time.place(x=1150, y=10)

##        while True:
        a.add_item_label = tk.Label(a, text='', fg="#476581")
        a.add_item_label.pack()
        a.add_item_label_list = []
        a.update()
        a.add_item_label.update()


        a.columnconfigure(0, weight=1)
        a.rowconfigure(0, weight=1)

        a.tree = ttk.Treeview(a, show="headings", height=35)
        a.tree["show"] = "headings"
        a.tree["columns"] = list(range(4))
##        for i in range(4):
##            a.tree.column('#' + str(i), minwidth=150, stretch=0)
##            a.tree.heading(i, text="Meal {}".format(i))
        col_list = ['date', 'meal 1', 'meal 2', 'meal 3']
        for i, z in zip(range(4), col_list):

            a.tree.column('#' + str(i), minwidth=150, stretch=0)
            a.tree.heading(0, text="Meal 0 / Date")
            a.tree.heading(i, text="Meal {}".format(i))
##        a.tree.column("date", minwidth=160, stretch=0)    
##        a.tree.heading(0, text="date")
##        a.tree.column("date2", minwidth=160,stretch=0)    
##        a.tree.heading(1, text="date2")
##        a.tree.column("date2", minwidth=160, stretch=0)    
##        a.tree.heading(2, text="date2")
##        a.tree.column("date3", minwidth=160,stretch=0)    
##        a.tree.heading(3, text="date3")
##        
               
##
##        a.tree.column('#', minwidth=300, stretch=0)
##        a.tree.heading(a.tree, text="Date")
##        for i in range(5):
##            tree.insert('', "end", i)

        a.tree.pack()
        a.tree.place(x=500,y=35)
        xs = ttk.Scrollbar(a, orient=tk.HORIZONTAL, command=a.tree.xview)

        #### to be implemented later, use grid? ####       
##        a.tree["xscrollcommand"] = xs.set
##        xs.pack()
##        xs.place(x=500, y=243)

        

        a.variable = tk.StringVar(a)
        a.stringvar_housing = tk.StringVar(a)
        a.stringvar_voices = tk.StringVar(a)
        a.stringvar_thoughts = tk.StringVar(a)
        a.stringvar_substance = tk.StringVar(a)
        a.stringvar_meds = tk.StringVar(a)
        a.stringvar_meds2 = tk.StringVar(a)
        a.stringvar_head = tk.StringVar(a)
        a.entry_buy = tk.Entry(a)
        a.stringvar_label = tk.StringVar(a)
        a.stringvar_hospital = tk.StringVar(a)
        a.stringvar_music = tk.StringVar(a)
        a.entry_movies = tk.Entry(a)
        a.stringvar_exercise = tk.StringVar(a)
        a.entry_programming = tk.Entry(a)
        a.stringvar_scrub = tk.StringVar(a)
        a.stringvar_bigd = tk.StringVar(a)
        a.stringvar_sleep = tk.StringVar(a)
        a.stringvar_sleep2 = tk.StringVar(a)
        a.stringvar_pray = tk.StringVar(a)
##        a.variable = 0

##        a.var = [1,2,3,4,1,2,3,4,1,2,3,4,1,2,3,4,]
##        a.var2 = ["a","a","a","a","a","a","a","a","a","a",
##                  ]
##
##        for z, a in zip(a.var, a.var2):
##        
##            tree.insert('', 'end', text="Item_" + str(z), values=("mg", a, "z", "aaaaa"))
##        a.variable += 1

##        a.mainloop()
##        a.add_item_label.place(x=300 + a.increment,y=40)
        
##        print('add item event', event)
##        tk.Bind_Class("special", "<Button>", a.edit)

        
        
##    def retag(a, tag, *args):
##        '''Add the given tag as the first bindtag for every widget passed in'''
##        for widget in args:
##            widget.bindtags((tag,) + widget.bindtags())

    def spreadsheet(a):
        count = 0
        #with open("/home/c/Desktop/py/good_food.csv", "r") as infile, open("/home/c/Desktop/py/good_food_v2.csv", "a") as outfile:
         
        with open("good_food.csv", "r") as infile, open("good_food_v2.csv", "a") as outfile:
           reader = csv.reader(infile)
           for line in reader:
               count += 1
           for z in range(0, count):
               next(reader, None)
##           next(reader, None)  # skip the headers
           writer = csv.writer(outfile)
##           for row in reader:
               # process each row
##           number_of_k = a.count_d_k(d = a.itemsd)
##           if number_of_k == 1:
##               from time import gmtime, strftime, localtime

##        time = strftime("%Y-%m-%d %H:%M:%S", localtime())
##               a.time = strftime("%Y-%m-%d %H:%M:%S", localtime()) 
           writer.writerow([a.entry_spreadsheet.get(),])
           
        a.listbox_update_v2()

    def listbox_update_v2(a):
        a.listbox.delete(0, 'end')

        # sorting data 
##            data = sorted(data, key=str.lower)

        # put new data
##            for item in data:
        a.listbox.insert('end', a.entry_spreadsheet.get())
    
    def back_a_meal(a):
        a.incre_var -= 1
    def edit_helper(a):
        selected_item = a.tree.selection()[0]
        print('now editing ' + selected_item)
        a.tree.item(selected_item, text=a.event, values=a.event)
    def edit(a):
##        pass
        
        selected_item = a.tree.selection()[0]
        print('now editing ' + selected_item)
##        a.tree.insert('', "end", selected_item)
##        a.tree.delete(selected_item)

        
##        a.retag("special", popup_box)
        
        
        
        popup_box = tk.Toplevel(a, height=600, width=600)
        entry = tk.Entry(popup_box)
        entry.pack()
        entry.bind('<KeyRelease>', a.on_keyrelease2)
        entry.place(x=250, y=5)

##        a.retag("special", popup_box)
        
        a.listbox = tk.Listbox(popup_box)
        a.listbox.pack()
        #listbox.bind('<Double-Button-1>', on_select)
        a.listbox.bind('<<ListboxSelect>>', a.on_select2)
        a.listbox_update(a.food_database)
        a.listbox.place(x=250, y=20)
##        def popup_edit(a):
            
            
        button5 = tk.Button(popup_box, text = 'Edit', command=a.edit_helper)
        button5.configure(width = 10, activebackground = "#33B5E5",
                          bg='#476581', font=("Helvetica", 10))    
        button5.place(x=5, y=5)
        button5.config(highlightthickness=0, fg='white')
        
              
##        z = a.tree.get_children()
##        for item in z: ## Changing all children from root item
##            a.tree.item(item, text="a", values=("a", "aaaaaaaaa"))
        
    def count_d_k(a, d):
        count = 0
        for key in d.keys():
            if key:
                count += 1
        return(count)
    
    def export(a):
        wn_check = tk.Toplevel(a, height=1080, width=1920)
        label_increment = 5

        
        num_one_ten = ['', 'not answering', 'dont know', ]
        for number in range(0,11):
            if number == 6 or number == 7:
                next
            else:
                num_one_ten.append(number)
            
##        num_one_ten.append('not answering')
        
        yes_no = ['', 'not answering', 'yes', 'dont know', 'no']

        parents_q_a = ['', 'not answering', 'great', 'good', 'so so',
                       'not good', 'terrible', 'dont know']

##        variable = tk.StringVar(a)
##        variable.set(num_one_ten[0]) # default value
##
##        w = tk.OptionMenu(wn_check, variable, *num_one_ten)
##        w.pack()
##        w.place(x=325, y=320)

##        s = ttk.Style(wn_check)
##        s.configure("OptionMenu", background="#476581")
####        s.configure("TOptionMenu", background="grey")

        second_set_qs_x = 425

##        a.entry_pray.pack()
####        entry.bind('<KeyRelease>', a.on_keyrelease)
##        a.entry_pray.place(x=second_set_qs_x, y=375)

##        label_account = tk.Label(wn_check, text="What is your bank account balance?", fg="#476581")
##        label_account.pack()
##        label_account.place(x=400,y=155)
####        a.stringvar_meds = tk.StringVar(a)
####        a.stringvar_meds.set(yes_no[0]) # default value
##        a.entry_accounts = tk.Entry(wn_check)
##        a.entry_accounts.pack()
####        entry.bind('<KeyRelease>', a.on_keyrelease)
##        a.entry_accounts.place(x=400, y=170)
##        optionmenu_meds = tk.OptionMenu(wn_check, a.stringvar_meds, *yes_no)
####        optionmenu_meds.config( fg="#476581")
##        optionmenu_meds.pack()
##        optionmenu_meds.place(x=735, y=105)
        
        
##        a.second_export(a.stringvar_bigd.get(), a.stringvar_scrub.get())
##        with open('food_spreadsheet_v2.csv','w') as f:
####            f.write('date, meal0, meal1, meal2, meal3\n'','','','',''\n')
##            f.write('date, meal0, meal1, meal2, meal3, housing, voices, thoughts, substances, anti-depressant, head, purchases, stress, hospital, music, movies, programming, scrub, bigd\n')
##
##        with open('food_spreadsheet_v2.csv','a',newline='') as f:
##            writer = csv.writer(f)
####            for z in range(0,6):
##            number_of_k = a.count_d_k(d = a.itemsd)
##            if number_of_k == 1:
##                writer.writerow([a.time, a.itemsd[1], 0, 0,0])
##            elif number_of_k == 2:
##                writer.writerow([a.time, a.itemsd[1], a.itemsd[2], 0,0])
##            elif number_of_k == 3:
##                writer.writerow([a.time, a.itemsd[1], a.itemsd[2], a.itemsd[3],0])
##            else:
##                writer.writerow([a.time, a.itemsd[1], a.itemsd[2], a.itemsd[3],a.itemsd[4]])
##            
##           
####            writer.writerow([0,0,0])
####            writer.writerow([0,0,0])
##        with open('food_spreadsheet_v2.csv') as f:
##             print(f.read())

        
##        with open('food_spreadsheet.csv', 'w') as file:  # Just use 'w' mode in 3.x
##            w = csv.DictWriter(file, a.itemsd.keys())
##            w.writeheader()
##            w.writerow(a.itemsd)
    def clear_all(a):
        a.tree.delete(*a.tree.get_children())
        
    def history(a):
        ## examples/get_csv_data.py
        ## if len(a.itemsdr):
##        a.tree.de    
        
        a.tree.delete(*a.tree.get_children()) ## does this delete all items in list
        import csv
        count = 0
        food_database = []
        food_names = []
        food_dates = []
        new_food_dates = []
        def food_list_to_dict(meal):
            with open('/home/c/Desktop/py/food_spreadsheet_v3.csv') as file:
                file_csv = csv.DictReader(file)
            ##    something = next(file_csv)
            ##    print(file_csv)
            ##    for col in file_csv:
            ##        print(col)
                for row in file_csv:
                    try:
                        
            ##                    print(row[0])
                        food_dates.append(row['date'])
                        food_names.append(row[meal])
            ##            food_database.append(row.keys())
            ##            food_database.append(row[' meal1'])
            ##            food_database.append(row[' meal2'])
            ##            food_database.append(row[' meal3'])
                    except IndexError:
                        count += 1               
                        next
##            for item in food_dates:
##                a = item.replace(item, item + meal)
##        ##        print('****** a is ', a)
##        ##        food_dates = []
##                new_food_dates.append(a)
##            print('** new food dates **', food_dates)
            ##print(food_database[1])
            itemsdr = {}
            for z, k in zip(food_names, food_dates):
                itemsdr[k] = z
            return(itemsdr)
##            print('---- itemsdr -----', itemsdr)
##            print(itemsdr)
            
            ## see examples/keylen.py
        def make_tree(d, string):
            for key, value in d.items():
    ##            for value in meal0.items():
    ##            a.tree.insert('', 'end', text=a.event, values=(key, '', '', ''))
                   
                if string == 'meal0':
                    a.tree.insert('', 'end', text=a.event, values=(key,value,'',''))
                elif string == 'meal1':
                    a.tree.insert('', 'end', text=a.event, values=(key,'',value,''))
                else:
                    a.tree.insert('', 'end', text=a.event, values=(key,'','',value))
                
        meal0 = food_list_to_dict(' meal0')
        make_tree(meal0, "meal0")
        meal1 = food_list_to_dict(' meal1')
        make_tree(meal1, "meal1")
        meal2 = food_list_to_dict(' meal2')
        make_tree(meal2, "meal2")

##        print('** meal0 **', meal0)
##        print('((((meal 1 )))))', meal1)
##        import pprint
##        print('^^^^^^ meal 2 ^^^^^, ', meal2)

##        z = {**meal0, **meal1, **meal2}
##        final_dict = {**z, **meal2}
##        print('*** meal 2 *** ' , meal2)

        def merge_dicts(*dict_args):
            """
            Given any number of dicts, shallow copy and merge into a new dict,
            precedence goes to key value pairs in latter dicts.
            """
            result = {}
            for dictionary in dict_args:
                result.update(dictionary)
            return result
##                print(z)
##        zee = merge_dicts(meal0,meal1,meal2)
##        print(zee)
        
##        print('meal0 is ', meal0)

        
##            a.tree.insert('', 'end', text=a.event, values=('', value, '', ''))



            
##            meal1 = food_list_to_dict(' meal1')
##            meal2 = food_list_to_dict(' meal2')
##            meal3 = food_list_to_dict(' meal3')
##        import csv
##
##        food_database = []
##        with open('/home/c/Desktop/py/food_spreadsheet_v3.csv') as file:
##            file_csv = csv.DictReader(file)
####    something = next(file_csv)
####    print(file_csv)
####    for col in file_csv:
####        print(col)
##            for row in file_csv:
##                try:
##            
####                    print(row[0])
##                    food_database.append(row[' meal0'])
##                except IndexError:
##                    next
####print(food_database)
####print(list(food_database[1]))
##
####for z in food_database:
####    a = ''.join(z)
######    a = list(a)
####    print(a)
##        new_lst = []
##        itemsdr = {}
##        s = ''
##        for z in food_database:
##                ##    print(z)
##            for pos, zee in enumerate(z):
##                ##        if zee == "'":
##                ##            position = pos
##                ##        else:
##                new_lst.append(zee)
##        ##print(new_lst)
##        my_lst_str = ''.join(map(str, new_lst))
##        print(type(my_lst_str))
##
##        new = ''
##        for z in my_lst_str:
##            if z=="'":
##                next
##            else:
##                new += z
##                print(new)
##
##        positions = []
##
##        for pos, z in enumerate(new):
##            if z == "[" or z == "]":
##                positions.append(pos)
##
##        good_lst = []
##        ##word = ''
##        for pos, z in enumerate(positions):
##            if pos % 2 == 0:
##                z = z + 1
##        
####        print('new z (z += 1 ) is', new_z)
##
##                good_lst.append(new[z:positions[pos+1]])
##        
##        for z, zee in enumerate(good_lst):
##            itemsdr[z + 1] = zee

            
##        if len(itemsdr) == 1:
##            a.tree.insert('', 'end', text=a.event, values=(itemsdr[1], '', '', ''))
##        elif len(itemsdr) == 2:
##            a.tree.insert('', 'end', text=a.event, values=(itemsdr[1], itemsdr[2], '', ''))
##        elif len(itemsdr) == 3:
##            a.tree.insert('', 'end', text=a.event, values=(itemsdr[1], itemsdr[2], itemsdr[3], ''))
##        else:
##            a.tree.insert('', 'end', text=a.event, values=(itemsdr[1], itemsdr[2], itemsdr[3], itemsdr[4]))



##        if a.incre_var == 1:
##            a.itemsd[a.incre_var] = a.meal1_items
##            a.meal1_items.append(a.event)
##            a.tree.insert('', 'end', text=a.event, values=(a.event, '', '', ''))
##        elif a.incre_var == 2:
##            a.itemsd[a.incre_var] = a.meal2_items
##            a.meal2_items.append(a.event)
##            a.tree.insert('', 'end', text=a.event, values=('',a.event, '', ''))
##        elif a.incre_var == 3:
##            a.itemsd[a.incre_var] = a.meal3_items
##            a.meal3_items.append(a.event)
##            a.tree.insert('', 'end', text=a.event, values=('','',a.event))
##        elif a.incre_var == 4:
##            a.itemsd[a.incre_var] = a.meal4_items
##            a.meal4_items.append(a.event)
##            a.tree.insert('', 'end', text=a.event, values=('','','',a.event))
        
    def second_export(a):
##        print('getting musics value... ', a.stringvar_music.get())
####        print(a.stringvar_bigd.get(), a.stringvar_scrub.get(), a.stringvar_movies.get())
##        print(*args, **kw)

        ## ONLY NEED THIS ONCE YA?        
##        with open('food_spreadsheet_v3.csv','w') as f:
####            f.write('date, meal0, meal1, meal2, meal3\n'','','','',''\n')
##            f.write('date, meal0, meal1, meal2, meal3, housing, voices, thoughts, substances, Anti psychotic, anti-depressant,   head, ((BUY)), stress, hospital, music, movies, exercise, programming(entry), programming2, scrub, bigd, sleep1, sleep2, speaking, mood, mood2, anti-depressant2, qcc\n')

        count = 0
##        with open('/home/c/Desktop/py/food_spreadsheet_v2.csv', newline='') as f:
##            row = csv.reader(f, delimiter='\t')
##            for line in row:
##                count += 1
##            for z in range(0, count):
##                next(row, None)

        with open("food_spreadsheet_v2.csv", "r") as infile, open("food_spreadsheet_v3.csv", "a") as outfile:
           reader = csv.reader(infile)
           for line in reader:
               count += 1
           for z in range(0, count):
               next(reader, None)
##           next(reader, None)  # skip the headers
           writer = csv.writer(outfile)
##           for row in reader:
               # process each row
           number_of_k = a.count_d_k(d = a.itemsd)
           import requests

##           from bs4 import BeautifulSoup
##           html = BeautifulSoup(requests.get('http://markets.businessinsider.com/commodities/oil-price?type=wti').text,"html.parser" )
##           bs=html.find('span', {'class': 'price-row-price' })
##        ##print(bs)
##           for z in bs.strings:
##                price = z
        
           if number_of_k == 1:
               from time import gmtime, strftime, localtime

##        time = strftime("%Y-%m-%d %H:%M:%S", localtime())
               a.time = strftime("%Y-%m-%d %H:%M:%S", localtime()) 
               writer.writerow([a.time, a.itemsd[1], '', '','', ## a.variable.get(), ## commented out 
                a.stringvar_housing.get(), ## check x2
                a.stringvar_voices.get(),  ## check x2 
                a.stringvar_thoughts.get(), ## check x2
                a.stringvar_substance.get(), ## check x2
                a.stringvar_meds.get(), ## check x2
                a.stringvar_meds2.get(), ## check x2
                a.stringvar_head.get(), ##check x2
                a.entry_buy.get(), 
                a.stringvar_stress.get(), ## check x2
                a.stringvar_hospital.get(), ## check x2
                a.stringvar_music.get(), ## check x2
               ## a.stringvar_label.get(), ## ??
                a.entry_movies.get(), ## check x2
                a.stringvar_exercise.get(), ## check x2
                a.entry_programming.get(), ## check x2
                a.stringvar_programming.get(), ## check x2
                
               
                a.stringvar_scrub.get(),
                a.stringvar_bigd.get(),
                a.stringvar_sleep.get(),
                a.stringvar_sleep2.get(),
                a.stringvar_speaking.get(),
                a.stringvar_mood.get(),
                a.stringvar_mood2.get(),
                                a.entry_meds2.get(),
                                a.entry_qcc.get(),
                                a.stringvar_weapons.get(),
                                a.stringvar_pray.get(),
                                # price

                                
                ## a.stringvar_bigd.get()
                                ])
           elif number_of_k == 2:
                writer.writerow([a.time, a.itemsd[1], a.itemsd[2], '','',## a.variable.get(), ## commented out 
                a.stringvar_housing.get(), ## check x2
                a.stringvar_voices.get(),  ## check x2 
                a.stringvar_thoughts.get(), ## check x2
                a.stringvar_substance.get(), ## check x2
                a.stringvar_meds.get(), ## check x2
                a.stringvar_meds2.get(), ## check x2
                a.stringvar_head.get(), ##check x2
                a.entry_buy.get(), 
                a.stringvar_stress.get(), ## check x2
                a.stringvar_hospital.get(), ## check x2
                a.stringvar_music.get(), ## check x2
               ## a.stringvar_label.get(), ## ??
                a.entry_movies.get(), ## check x2
                a.stringvar_exercise.get(), ## check x2
                a.entry_programming.get(), ## check x2
                a.stringvar_programming.get(), ## check x2
                
               
                a.stringvar_scrub.get(),
                a.stringvar_bigd.get(),
                a.stringvar_sleep.get(),
                a.stringvar_sleep2.get(),
                                 a.stringvar_speaking.get(),
                a.stringvar_mood.get(),
                a.stringvar_mood2.get(),
                                a.entry_meds2.get(),
                                a.entry_qcc.get(),
                                a.stringvar_weapons.get(),
                                a.stringvar_pray.get(),
                ## a.stringvar_bigd.get()
                                ])
                ## a.stringvar_bigd.get()
                            
           elif number_of_k == 3:
                writer.writerow([a.time, a.itemsd[1], a.itemsd[2], a.itemsd[3],'', ## a.variable.get(), ## commented out 
                a.stringvar_housing.get(), ## check x2
                a.stringvar_voices.get(),  ## check x2 
                a.stringvar_thoughts.get(), ## check x2
                a.stringvar_substance.get(), ## check x2
                a.stringvar_meds.get(), ## check x2
                a.stringvar_meds2.get(), ## check x2
                a.stringvar_head.get(), ##check x2
                a.entry_buy.get(), 
                a.stringvar_stress.get(), ## check x2
                a.stringvar_hospital.get(), ## check x2
                a.stringvar_music.get(), ## check x2
               ## a.stringvar_label.get(), ## ??
                a.entry_movies.get(), ## check x2
                a.stringvar_exercise.get(), ## check x2
                a.entry_programming.get(), ## check x2
                a.stringvar_programming.get(), ## check x2
                
               
                a.stringvar_scrub.get(),
                a.stringvar_bigd.get(),
                a.stringvar_sleep.get(),
                a.stringvar_sleep2.get(),
                ## a.stringvar_bigd.get()
                            a.stringvar_speaking.get(),
                a.stringvar_mood.get(),
                a.stringvar_mood2.get(),
                                a.entry_meds2.get(),
                                a.entry_qcc.get(),
                                a.stringvar_weapons.get(),
                                a.stringvar_pray.get(),
                ## a.stringvar_bigd.get()
                                ])
           elif number_of_k == 4:
           
           
                writer.writerow([a.time, a.itemsd[1], a.itemsd[2], a.itemsd[3],a.itemsd[4], ## a.variable.get(), ## commented out 
                a.stringvar_housing.get(), ## check x2
                a.stringvar_voices.get(),  ## check x2 
                a.stringvar_thoughts.get(), ## check x2
                a.stringvar_substance.get(), ## check x2
                a.stringvar_meds.get(), ## check x2
                a.stringvar_meds2.get(), ## check x2
                a.stringvar_head.get(), ##check x2
                a.entry_buy.get(), 
                a.stringvar_stress.get(), ## check x2
                a.stringvar_hospital.get(), ## check x2
                a.stringvar_music.get(), ## check x2
               ## a.stringvar_label.get(), ## ??
                a.entry_movies.get(), ## check x2
                a.stringvar_exercise.get(), ## check x2
                a.entry_programming.get(), ## check x2
                a.stringvar_programming.get(), ## check x2
                
               
                a.stringvar_scrub.get(),
                a.stringvar_bigd.get(),
                a.stringvar_sleep.get(),
                a.stringvar_sleep2.get(),
                                 a.stringvar_speaking.get(),
                a.stringvar_mood.get(),
                a.stringvar_mood2.get(),
                                a.entry_meds2.get(),
                                a.entry_qcc.get(),
                                a.stringvar_weapons.get(),
                                a.stringvar_pray.get(),
                ## a.stringvar_bigd.get()
                               ])
           elif number_of_k == 0:
                writer.writerow([a.time, '', '', '', '',  a.stringvar_housing.get(), ## check x2
                a.stringvar_voices.get(),  ## check x2 
                a.stringvar_thoughts.get(), ## check x2
                a.stringvar_substance.get(), ## check x2
                a.stringvar_meds.get(), ## check x2
                a.stringvar_meds2.get(), ## check x2
                a.stringvar_head.get(), ##check x2
                a.entry_buy.get(), 
                a.stringvar_stress.get(), ## check x2
                a.stringvar_hospital.get(), ## check x2
                a.stringvar_music.get(), ## check x2
               ## a.stringvar_label.get(), ## ??
                a.entry_movies.get(), ## check x2
                a.stringvar_exercise.get(), ## check x2
                a.entry_programming.get(), ## check x2
                a.stringvar_programming.get(), ## check x2
                
               
                a.stringvar_scrub.get(),
                a.stringvar_bigd.get(),
                a.stringvar_sleep.get(),
                a.stringvar_sleep2.get(),
                                 a.stringvar_speaking.get(),
                a.stringvar_mood.get(),
                a.stringvar_mood2.get(),
                                a.entry_meds2.get(),
                                a.entry_qcc.get(),
                                a.stringvar_weapons.get(),
                                a.stringvar_pray.get(),
                ## a.stringvar_bigd.get()
                               ])
        ## start of older stuff 
##        with open('food_spreadsheet_v2.csv','a',newline='') as f:            
##            writer = csv.writer(f)
####            for z in range(0, count):
####                next(writer, None)
##            
####            for z in range(0,6):
##            number_of_k = a.count_d_k(d = a.itemsd)
##            if number_of_k == 1:
##                writer.writerow([a.time, a.itemsd[1], 0, 0,0, ## a.variable.get(), ## commented out 
##                a.stringvar_housing.get(), ## check x2
##                a.stringvar_voices.get(),  ## check x2 
##                a.stringvar_thoughts.get(), ## check x2
##                a.stringvar_substance.get(), ## check x2
##                a.stringvar_meds.get(), ## check x2
##                a.stringvar_meds2.get(), ## check x2
##                a.stringvar_head.get(), ##check x2
##                a.entry_buy.get(), 
##                a.stringvar_stress.get(), ## check x2
##                a.stringvar_hospital.get(), ## check x2
##                a.stringvar_music.get(), ## check x2
##               ## a.stringvar_label.get(), ## ??
##                a.entry_movies.get(), ## check x2
##                a.stringvar_exercise.get(), ## check x2
##                a.entry_programming.get(), ## check x2
##                a.stringvar_programming.get(), ## check x2
##                
##                
##                
##                
##                a.stringvar_scrub.get(), 
##                a.stringvar_bigd.get()])
##            elif number_of_k == 2:
##                writer.writerow([a.time, a.itemsd[1], a.itemsd[2], 0,0, a.variable.get(),
##                a.stringvar_housing.get(), 
##                a.stringvar_voices.get(), 
##                a.stringvar_thoughts.get(), 
##                a.stringvar_substance.get(),
##                a.stringvar_meds.get(), 
##                a.stringvar_meds2.get(), 
##                a.stringvar_head.get(), 
##                a.entry_buy.get(),
##                a.stringvar_label.get(), 
##                a.stringvar_hospital.get(), 
##                a.stringvar_music.get(), 
##                a.entry_movies.get(),
##                a.stringvar_exercise.get(), 
##                a.entry_programming.get(),
##                a.stringvar_scrub.get(), 
##                a.stringvar_bigd.get()])
##            elif number_of_k == 3:
##                writer.writerow([a.time, a.itemsd[1], a.itemsd[2], a.itemsd[3],0, a.variable.get(),
##                a.stringvar_housing.get(), 
##                a.stringvar_voices.get(), 
##                a.stringvar_thoughts.get(), 
##                a.stringvar_substance.get(),
##                a.stringvar_meds.get(), 
##                a.stringvar_meds2.get(), 
##                a.stringvar_head.get(), 
##                a.entry_buy.get(),
##                a.stringvar_label.get(), 
##                a.stringvar_hospital.get(), 
##                a.stringvar_music.get(), 
##                a.entry_movies.get(),
##                a.stringvar_exercise.get(), 
##                a.entry_programming.get(),
##                a.stringvar_scrub.get(), 
##                a.stringvar_bigd.get()])
##            else:
##                writer.writerow([a.time, a.itemsd[1], a.itemsd[2], a.itemsd[3],a.itemsd[4], a.variable.get(),
##                a.stringvar_housing.get(), 
##                a.stringvar_voices.get(), 
##                a.stringvar_thoughts.get(), 
##                a.stringvar_substance.get(),
##                a.stringvar_meds.get(), 
##                a.stringvar_meds2.get(), 
##                a.stringvar_head.get(), 
##                a.entry_buy.get(),
##                a.stringvar_label.get(), 
##                a.stringvar_hospital.get(), 
##                a.stringvar_music.get(), 
##                a.entry_movies.get(),
##                a.stringvar_exercise.get(), 
##                a.entry_programming.get(),
##                a.stringvar_scrub.get(), 
##                a.stringvar_bigd.get()])
            
           
##            writer.writerow([0,0,0])
##            writer.writerow([0,0,0])
        with open('food_spreadsheet_v3.csv') as f:
             print(f.read())
    
    def delete(a):
        selected_item = a.tree.selection()[0] ## get selected item
        a.tree.delete(selected_item)
        
    def remove_itemz(a):
        a.tree.delete(a)
##        a.add_item_label['text'] =  ''   # update label
##        a.after(100, a.remove_itemz)
##        entry.delete(0, END)
    def remove_item(a):
        meal_list = [a.meal1_items,
        a.meal2_items, 
        a.meal3_items ,
        a.meal4_items ,
        a.meal5_items ,
        a.meal6_items ,
        
        a.meal8_items]

        for meal in meal_list:
            for z in meal:
                print('on list number ', meal)
                print('z is ', z)
##                if len(meal) > 0:
    ##                meal = ''
##                    print('a.items in remove_item before ', a.items)
##                meal.pop(0)
                a.add_item_label.update()
                meal.clear()
                print('meal is ', meal)
##                z = ''
                print('a.add_item_label', a.add_item_label)
##        for z in a.add_item_label_list:
##            z=''
##            a.add_item_label.configure(text=z)
##                a.add_item_label.configure(text=z)
##                z = ''
                
##                    print('a.items in remove_item before ', a.items)
    ##                a.label_time.update()
##                else:
##                    print('empty list')
        
        
    
    def relax(a):
        var = 1+1

    def create_widgets(a):
        pass
##        a.add_meals = tk.Button(a)
##        a.add_meals["text"] = "Add Meal"
##        a.add_meals["command"] = a.add_meal()
##        a.add_meals.pack(side="top")

        

        

        

##        a.quit = tk.Button(a, text="QUIT", fg="red",
##                              command=a.zero.destroy)
##        a.quit.pack(side="bottom")
##    def new_meal_helper(a):
##        print('new meal helper a.event is', a.event)

    def new_meal_v2(a):
        a.incre_var += 1

        
    def new_meal(a):
        print(a.itemsd)
##        try:
##            a.event_new_meal = a.event
##        except Exception:
####            a.relax()
##
##            
####            a.event = ''
##            a.items = []
##    ##        a.event = event.widget.get(event.widget.curselection())
##            print('a.event is ', a.event)
####            print('a.event_new_meal is ', a.event_new_meal)
##    ##        lst = [1,2,3,4,5,6]
##    ##        a.incre_var = 1
##    ##        a.increment = 0
##    ##        for items in lst:
##            a.incre_var += 1
##            a.increment += 115
##            label = tk.Label(a, text="Meal " + str(a.incre_var), fg="#476581")
##            label.pack()
##            label.place(x=300 + a.increment,y=40)

            
##        a.event = ''
        a.items = []
##        a.event = event.widget.get(event.widget.curselection())
##        print('a.event in new meal is ', a.event)
##            print('a.event_new_meal is ', a.event_new_meal)
##        lst = [1,2,3,4,5]
##        a.incre_var = 1
##        a.increment = 0
##        for items in lst:
        
        a.increment += 115
        if a.incre_var < 9: # and a.increment < 1000:
            label = tk.Label(a, text="Meal " + str(a.incre_var), fg="#476581")
            label.pack()
            
            label.place(x=300 + a.increment,y=40)
        else:
            print('no more than 8 meals')
    def add_item_update(a):
        pass
    
    def add_item_v2(a):
        
        
        if a.incre_var == 1:
            a.itemsd[a.incre_var] = a.meal1_items
            a.meal1_items.append(a.event)
            a.tree.insert('', 'end', text=a.event, values=(a.event, '', '', ''))
        elif a.incre_var == 2:
            a.itemsd[a.incre_var] = a.meal2_items
            a.meal2_items.append(a.event)
            a.tree.insert('', 'end', text=a.event, values=('',a.event, '', ''))
        elif a.incre_var == 3:
            a.itemsd[a.incre_var] = a.meal3_items
            a.meal3_items.append(a.event)
            a.tree.insert('', 'end', text=a.event, values=('','',a.event))
        elif a.incre_var == 4:
            a.itemsd[a.incre_var] = a.meal4_items
            a.meal4_items.append(a.event)
            a.tree.insert('', 'end', text=a.event, values=('','','',a.event))
##        elif a.incre_var == 5:
##            a.itemsd[a.incre_var] = a.meal5_items
##            a.meal5_items.append(a.event)
##        elif a.incre_var == 6:
##            a.itemsd[a.incre_var] = a.meal6_items
##            a.meal6_items.append(a.event)
##        elif a.incre_var == 7:
##            a.itemsd[a.incre_var] = a.meal7_items
##            a.meal7_items.append(a.event)
##        else:
##            a.itemsd[a.incre_var] = a.meal8_items
##            a.meal8_items.append(a.event)
        print(a.itemsd)
##        a.tree.insert('', 'end', text=a.incre_var, values=a.event)
        
    def add_item(a):
##        print('add_meal()')
##        try:
##        a.event = a.on_select()        
##        a.event_bool = True
##        if boolean:
        if a.incre_var == 0:
            print('new meal first')
        else:
            print('a event in add item', a.event)
        ##        meal =
            a.items.append(a.event)
##            a.meal1_items.append(a.event)
            
##            a.itemsd = ([a.event], [a.incre_var])
##            print(a.itemsd)
##            a.itemsd = FrozenDict()
##            a.itemsd[a.event] = a.incre_var
##            a.itemsd([(a.event, a.incre_var)])
##            helper_list = []
##            meal_check = a.incre_var
##            mealz = [1,2,3,4,5,6,7,8]
##            for meal in mealz:
##                a.itemsd[a.incre_var] = a.mealmeal_items

            if a.incre_var == 1:
                a.itemsd[a.incre_var] = a.meal1_items
                a.meal1_items.append(a.event)
            elif a.incre_var == 2:
                a.itemsd[a.incre_var] = a.meal2_items
                a.meal2_items.append(a.event)
            elif a.incre_var == 3:
                a.itemsd[a.incre_var] = a.meal3_items
                a.meal3_items.append(a.event)
            elif a.incre_var == 4:
                a.itemsd[a.incre_var] = a.meal4_items
                a.meal4_items.append(a.event)
            elif a.incre_var == 5:
                a.itemsd[a.incre_var] = a.meal5_items
                a.meal5_items.append(a.event)
            elif a.incre_var == 6:
                a.itemsd[a.incre_var] = a.meal6_items
                a.meal6_items.append(a.event)
##            elif a.incre_var == 7:
##                a.itemsd[a.incre_var] = a.meal7_items
##                a.meal7_items.append(a.event)
            else:
                a.itemsd[a.incre_var] = a.meal8_items
                a.meal8_items.append(a.event)
          
           
                

            print(a.itemsd)
##            a.new_meal_helper()
        ##        a.event = a.event.widget.get(a.event.widget.curselection())
        ##        label = tk.Label(a, text="Meal 1", fg="#476581")
        ##        label.pack()
        ##        label.place(x=790,y=40)
            
            increment = 0
            all_meal_items = [a.meal1_items,
        a.meal2_items, 
        a.meal3_items ,
        a.meal4_items ,
        a.meal5_items ,
        a.meal6_items ,
        
        a.meal8_items]
            for item in a.items:
                increment = increment + 35 
                
                a.add_item_label = tk.Label(a, text=item, fg="#476581")
                
                a.add_item_label.pack()
                a.add_item_label_list.append(a.add_item_label)
                if a.increment < 1000: # increment < 281: # and a.increment < 1000:
                    a.add_item_label.place(x=300 + a.increment,y=40 + increment)
                    
##                    label.place(x=300 + a.increment,y=40 + increment)
##                    v = tk.StringVar()
##                    z = tk.Label(a, textvariable=v).pack()
##                    v.set(item)
##                    print(v)
##                    z.place(x=300 + a.increment,y=40 + increment)
                else:
##                    print('off screen')
##                    label.place(x=300 + 920,y=40 + 280) #meal 8
                    label.place(x=300 + 920,y=40 + increment) #meal 8
        
##        if boolean == True:
##            print('add item', event)
##        print('add item', event)
        
##        button1 = tk.Button(a, text = 'Add Item', command=a.relax)
##        button1.configure(width = 10, activebackground = "#33B5E5",
##                          bg='#476581', font=("Helvetica", 10))
##    
####            button1_window = a.canvas1.create_window(35, 10 + increment, anchor=NW, window=button1)
####        button1_window = a.canvas1.create_window(100, 25,  window=button1)
##        button1.place(x=580, y=40)
##        button1.config(highlightthickness=0, fg='white')
        
##        return event
##        except:
##            pass
        
    
    def on_keyrelease(a, event):

        # get text from entry
        value = event.widget.get()
        value = value.strip().lower()

        # get data from test_list
        if value == '':
            print('a food db in key release method', a.food_database)
            data = a.food_database
        else:
            data = []
            for item in a.food_database:
                if value in item.lower():
                    data.append(item)
                    
        if len(data) == 1:
            a.listbox.select_set(0) #This only sets focus on the first item.
##            a.listbox.focus(to_focus)
            a.listbox.event_generate("<<ListboxSelect>>")
        # update data in listbox
        a.listbox_update(data)

        


    def listbox_update(a, data):
        # delete previous data
        a.listbox.delete(0, 'end')

        # sorting data 
        data = sorted(data, key=str.lower)

        # put new data
        for item in data:
            a.listbox.insert('end', item)


    def on_select(a, event):
##        global event
        # display element selected on list
        print('(event) previous:', event.widget.get('active'))
        print('(event)  current:', event.widget.get(event.widget.curselection()))
##        print(dir(event.widget.get()))
##        while True:
        a.event = event.widget.get(event.widget.curselection())
##
##    def on_keyrelease2(a, event):
##
##        # get text from entry
##        value = event.widget.get()
##        value = value.strip().lower()
##
##        # get data from test_list
##        if value == '':
##            
##            data = a.food_database
##            
##        else:
##            data = []
##            for item in a.food_database:
##                if value in item.lower():
##                    data.append(item)                
##
##        # update data in listbox
##        a.listbox_update(data)
##
##
##    def listbox_update2(a, data):
##        # delete previous data
##        a.listbox.delete(0, 'end')
##
##        # sorting data 
##        data = sorted(data, key=str.lower)
##
##        # put new data
##        for item in data:
##            a.listbox.insert('end', item)
##
##
##    def on_select2(a, event):
####        global event
##        # display element selected on list
##        print('(event) previous:', event.widget.get('active'))
##        print('(event)  current:', event.widget.get(event.widget.curselection()))
####        while True:
##        a.event = event.widget.get(event.widget.curselection())
####        a.event_bool = True
####        a.add_item(boolean=a.event_bool, event=a.event)
####        if a.event_bool:
##            
####        print('a.event', a.event)
####        return event
####        print(current_selection)
####        a.add_item(event=a.event)
        
        
        
        print('---')

##root = tk.Tk()
app = Application()
app.title('a')
app.mainloop()

##
##app = Application()
##app.zero.maxsize(1000, 400)
##app.mainloop()


##print(dir(tk.Frame.configure()))



